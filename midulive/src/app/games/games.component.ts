import { Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-games',
  standalone: true,
  imports: [],
  template: `
    Estos son los juegos favoritos de {{username }}
    <ul>
      @for (game of games; track game.id) {
        <li (click)="fav(game.name)"> {{game.name}}</li> 
      }      
    </ul>
  `,
  styles: ``
})
export class GamesComponent {
  @Input() username='';
  @Output() addFavoriteEvent = new EventEmitter<string>();
  games = [
    { id: 1,
      name: "Uncharted"},
    { id: 2, 
      name:  "Spiderman"}
  ]

  fav(gamename: string){
     this.addFavoriteEvent.emit(gamename);
  }
}
