import { Component } from '@angular/core';
import { GamesComponent } from '../games/games.component';

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [GamesComponent],
  templateUrl: './user.component.html',
  styleUrl: './user.component.css'
})
export class UserComponent {
  username= "luispz";
  isLoggedIn= false;
  favGame="";
  greet(){
    alert("socorro")
  }

  getFavorite(gamename: string){
    this.favGame = gamename;
  }



}
